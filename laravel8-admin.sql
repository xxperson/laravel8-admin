/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : laravel-flow

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 26/07/2022 16:46:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hp_branch
-- ----------------------------
DROP TABLE IF EXISTS `hp_branch`;
CREATE TABLE `hp_branch`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `type` smallint(6) NOT NULL DEFAULT 2 COMMENT '类型1:公司,2:部门',
  `orders` smallint(6) NOT NULL DEFAULT 0 COMMENT '排序',
  `parent_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '上级部门id',
  `created_at` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_branch
-- ----------------------------
INSERT INTO `hp_branch` VALUES (1, 'XXX石油集团公司', 2, 0, 0, 0, 1658825064);
INSERT INTO `hp_branch` VALUES (2, '油气勘探公司', 2, 0, 1, 1624351892, 1624351892);
INSERT INTO `hp_branch` VALUES (3, '钻井公司', 2, 0, 1, 1624351911, 1624351911);
INSERT INTO `hp_branch` VALUES (5, '工程技术部', 2, 0, 3, 1624702255, 1624702255);
INSERT INTO `hp_branch` VALUES (6, '安全运行部', 2, 0, 3, 1624702272, 1627633833);
INSERT INTO `hp_branch` VALUES (7, '财务资产部', 2, 0, 3, 1624702284, 1624702284);
INSERT INTO `hp_branch` VALUES (8, '油气勘探机关', 2, 0, 2, 1624702310, 1624702310);
INSERT INTO `hp_branch` VALUES (9, '石油勘探开发部', 2, 0, 2, 1624702323, 1624702323);
INSERT INTO `hp_branch` VALUES (10, '延长气田采气三厂', 2, 0, 2, 1624702338, 1624702338);
INSERT INTO `hp_branch` VALUES (11, '办公室', 2, 0, 8, 1624702360, 1624702360);
INSERT INTO `hp_branch` VALUES (12, '党委工作部', 2, 0, 8, 1624702373, 1624702373);
INSERT INTO `hp_branch` VALUES (13, '生产计划部', 2, 0, 8, 1624702388, 1624702388);
INSERT INTO `hp_branch` VALUES (14, '后勤服务中心', 2, 0, 11, 1624702407, 1624702407);

-- ----------------------------
-- Table structure for hp_config
-- ----------------------------
DROP TABLE IF EXISTS `hp_config`;
CREATE TABLE `hp_config`  (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置的key键名',
  `value` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置的val值',
  `inc_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置分组',
  `desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_config
-- ----------------------------

-- ----------------------------
-- Table structure for hp_jobs
-- ----------------------------
DROP TABLE IF EXISTS `hp_jobs`;
CREATE TABLE `hp_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_jobs
-- ----------------------------
INSERT INTO `hp_jobs` VALUES (1, 'test', '111', 123, NULL, 0, 0);

-- ----------------------------
-- Table structure for hp_login_log
-- ----------------------------
DROP TABLE IF EXISTS `hp_login_log`;
CREATE TABLE `hp_login_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '登录者名字',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '登录者id',
  `operating_system` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作系统',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '登录IP',
  `useragent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '登录时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '登录状态,0:失败,1:成功',
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '提示信息',
  `login_num` smallint(6) NOT NULL DEFAULT 0 COMMENT '登录次数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 234 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_login_log
-- ----------------------------
INSERT INTO `hp_login_log` VALUES (1, 'wusong', 17, 'Windows', '127.0.0.1', 'chrome', 1624527382, 0, 'Method Illuminate\\Http\\Request::put does not exist.', 0);
INSERT INTO `hp_login_log` VALUES (2, 'wusong', 17, 'Windows', '127.0.0.1', 'chrome', 1624527437, 0, 'Method Illuminate\\Http\\Request::put does not exist.', 0);
INSERT INTO `hp_login_log` VALUES (3, 'wusong', 17, 'Windows', '127.0.0.1', 'chrome', 1624527530, 0, 'Method Illuminate\\Http\\Request::put does not exist.', 0);
INSERT INTO `hp_login_log` VALUES (4, 'wusong', 17, 'Windows', '127.0.0.1', 'chrome', 1624527634, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (5, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624528875, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (6, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624581859, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (9, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624583441, 0, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (10, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624583620, 0, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (11, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624583893, 1, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (12, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624583893, 1, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (13, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624606436, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (14, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624607193, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (15, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624611762, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (16, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624613437, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (17, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624613533, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (18, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624640599, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (19, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624640604, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (20, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624690031, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (21, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624690039, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (22, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624700881, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (23, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624700886, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (24, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708703, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (25, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708706, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (26, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708775, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (27, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708785, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (28, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708803, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (29, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708822, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (30, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708905, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (31, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708923, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (32, '1971599474@qq.com', 0, 'Windows', '127.0.0.1', 'chrome', 1624708946, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (33, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624840852, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (34, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624927289, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (35, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1624930869, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (36, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624930897, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (37, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624930912, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (38, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624930918, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (39, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1624930929, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (40, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624930939, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (41, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1624930975, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (42, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624931295, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (43, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624931342, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (44, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1624933378, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (45, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624933513, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (46, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624944935, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (47, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1624960177, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (48, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625019903, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (49, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625040348, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (50, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625099696, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (51, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625472931, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (52, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625736365, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (53, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625791046, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (54, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625814447, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (55, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625844106, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (56, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625879102, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (57, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625936794, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (58, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625973790, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (59, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625973790, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (60, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1625992487, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (61, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1625993256, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (62, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626050333, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (63, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626051401, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (64, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626051456, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (65, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626078356, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (66, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626078403, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (67, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626079098, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (68, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626081819, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (69, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626081820, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (70, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626658582, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (71, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626770159, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (72, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626828793, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (73, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626839374, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (74, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626914565, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (75, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626934146, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (76, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626935014, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (77, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1626937750, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (78, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1626937969, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (80, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627000918, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (82, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627191836, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (83, 'beibei', 0, 'Windows', '127.0.0.1', 'chrome', 1627722633, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (84, 'beibei', 0, 'Windows', '127.0.0.1', 'chrome', 1627722638, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (85, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627722649, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (88, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627725921, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (89, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627726001, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (90, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627727732, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (91, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627732474, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (92, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627732477, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (93, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627732505, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (94, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627732527, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (95, 'guanyu', 0, 'Windows', '127.0.0.1', 'chrome', 1627732571, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (96, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627732574, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (97, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627864538, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (98, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627864765, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (99, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627866860, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (100, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627867263, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (101, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627867429, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (102, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627867962, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (103, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627868148, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (104, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627872189, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (105, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627872513, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (106, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627872529, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (107, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627872535, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (108, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627872539, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (109, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627872557, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (110, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627872853, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (111, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627872856, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (112, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627872883, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (113, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627884085, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (114, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627914137, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (115, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627951535, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (116, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627952891, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (117, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627953091, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (118, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627954195, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (119, 'liubei', 0, 'Windows', '127.0.0.1', 'chrome', 1627954199, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (120, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627954203, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (121, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1627957629, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (122, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627957639, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (123, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627976940, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (124, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627978726, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (125, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627979380, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (126, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1627979443, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (127, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627979861, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (128, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627980108, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (129, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1627982268, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (130, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628134246, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (131, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628134552, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (132, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628134553, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (133, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628134676, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (134, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628135259, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (135, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628135314, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (136, '刘备', 0, 'Windows', '127.0.0.1', 'chrome', 1628135743, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (137, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1628135747, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (138, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628135861, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (139, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628135927, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (140, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628141619, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (141, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628161300, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (142, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628211231, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (143, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628646729, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (144, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628671152, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (145, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628760445, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (146, 'liubei', 3, 'Windows', '127.0.0.1', 'chrome', 1628769353, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (147, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628770732, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (148, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628774523, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (149, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628815408, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (150, 'guanyu', 2, 'Windows', '127.0.0.1', 'chrome', 1628867337, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (151, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1628867382, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (152, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629275366, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (153, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629334767, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (154, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629447079, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (155, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629477445, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (156, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629507082, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (157, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629543711, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (158, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629557552, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (159, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629679056, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (160, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629699870, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (161, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629879230, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (162, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1629939859, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (163, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1630284189, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (164, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1630305284, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (165, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1630545317, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (166, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1630553324, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (167, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1634700646, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (168, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1634700650, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (169, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1634700876, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (170, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1635147611, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (171, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1635991988, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (172, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1636004568, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (173, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636004573, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (174, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1636005732, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (175, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636005736, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (176, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1636007877, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (177, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636007880, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (178, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636015193, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (179, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636072626, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (180, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636072635, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (181, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636078028, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (182, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636341077, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (183, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636530381, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (184, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636600806, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (185, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1636683192, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (186, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637117070, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (187, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637117098, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (188, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637134932, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (189, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637291681, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (190, 'admin', 1, 'Windows', '192.168.0.114', 'chrome', 1637294154, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (191, 'admin', 1, 'Windows', '192.168.0.128', 'chrome', 1637294271, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (192, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637302116, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (193, 'admin', 1, 'Windows', '192.168.0.114', 'chrome', 1637305115, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (194, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637543086, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (195, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637543524, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (196, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1637574079, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (197, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1638152907, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (198, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1638153265, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (199, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1638258865, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (200, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1639546490, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (201, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1639732989, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (202, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1639961725, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (203, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1639982803, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (204, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640054425, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (205, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640135219, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (206, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640135280, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (207, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640135393, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (208, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640136798, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (209, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1640944545, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (210, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1641029733, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (211, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1641206805, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (212, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1641206846, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (213, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1641882469, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (214, 'super_admin', 0, 'Windows', '127.0.0.1', 'chrome', 1647151776, 1, '用户不存在', 0);
INSERT INTO `hp_login_log` VALUES (215, 'admin', 0, 'Windows', '127.0.0.1', 'chrome', 1647151784, 1, '密码错误', 0);
INSERT INTO `hp_login_log` VALUES (216, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1647151788, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (217, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1650368362, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (218, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1650444131, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (219, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1650516187, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (220, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1651030688, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (221, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1652179167, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (222, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1652179384, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (223, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1653373524, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (224, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1657702060, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (225, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1657764542, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (226, 'admin', 1, 'Linux', '127.0.0.1', 'chrome', 1657790438, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (227, 'admin', 1, 'Linux', '127.0.0.1', 'chrome', 1657871473, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (228, 'admin', 1, 'Linux', '127.0.0.1', 'chrome', 1657871475, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (229, 'admin', 1, 'Linux', '127.0.0.1', 'chrome', 1657871475, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (230, 'admin', 1, 'Linux', '127.0.0.1', 'chrome', 1657871513, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (231, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1658113766, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (232, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1658113854, 0, '操作成功', 0);
INSERT INTO `hp_login_log` VALUES (233, 'admin', 1, 'Windows', '127.0.0.1', 'chrome', 1658823833, 0, '操作成功', 0);

-- ----------------------------
-- Table structure for hp_manager
-- ----------------------------
DROP TABLE IF EXISTS `hp_manager`;
CREATE TABLE `hp_manager`  (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `entry_password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '未加密密码',
  `email` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `salt` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '加密随机字符',
  `branch_id` tinyint(4) NOT NULL DEFAULT 0 COMMENT '部门id',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '启动状态1:启动,0:禁用',
  `last_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后台管理员最后一次登录ip',
  `last_time` int(11) UNSIGNED NULL DEFAULT NULL COMMENT '后台管理员最后一次登录时间',
  `created_at` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_manager
-- ----------------------------
INSERT INTO `hp_manager` VALUES (1, 'admin超级管理员', 'admin', 'b4b47762ff03b233ee902c996e9a0cc9', '123456', '1971599474@qq.com', '13297151131', 'c2DWJV', 0, 1, '127.0.0.1', 1658823833, 1624333894, 1658823833);
INSERT INTO `hp_manager` VALUES (2, '关羽', 'guanyu', '530a2ffab1e1ca62594d8302a1c30e4c', '123456', '19715113474@qq.com', '13297151131', 'unPFH0', 14, 1, '127.0.0.1', 1628867337, 1624930834, 1628867337);
INSERT INTO `hp_manager` VALUES (3, '刘备', 'liubei', '6b76cd4ea79e552b3a26e346d79d9a13', '123456', '2424445372@qq.com', '12345678910', 'orZCEx', 12, 1, '127.0.0.1', 1628769353, 1627191817, 1628769353);

-- ----------------------------
-- Table structure for hp_manager_role
-- ----------------------------
DROP TABLE IF EXISTS `hp_manager_role`;
CREATE TABLE `hp_manager_role`  (
  `manager_id` mediumint(9) NOT NULL DEFAULT 0 COMMENT '管理员id',
  `role_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '角色id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_manager_role
-- ----------------------------
INSERT INTO `hp_manager_role` VALUES (16, 4);
INSERT INTO `hp_manager_role` VALUES (17, 6);
INSERT INTO `hp_manager_role` VALUES (18, 3);
INSERT INTO `hp_manager_role` VALUES (18, 4);
INSERT INTO `hp_manager_role` VALUES (4, 3);
INSERT INTO `hp_manager_role` VALUES (4, 6);
INSERT INTO `hp_manager_role` VALUES (5, 3);
INSERT INTO `hp_manager_role` VALUES (5, 4);
INSERT INTO `hp_manager_role` VALUES (6, 3);
INSERT INTO `hp_manager_role` VALUES (6, 4);
INSERT INTO `hp_manager_role` VALUES (2, 6);
INSERT INTO `hp_manager_role` VALUES (3, 3);
INSERT INTO `hp_manager_role` VALUES (3, 4);

-- ----------------------------
-- Table structure for hp_migrations
-- ----------------------------
DROP TABLE IF EXISTS `hp_migrations`;
CREATE TABLE `hp_migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_migrations
-- ----------------------------

-- ----------------------------
-- Table structure for hp_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `hp_operate_log`;
CREATE TABLE `hp_operate_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '模块标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求方式',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'IP',
  `user_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '后台用户' COMMENT '操作用户类别',
  `request_params` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求参数',
  `response_params` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '返回参数',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '登录状态,0:失败,1:成功',
  `message` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '提示信息',
  `operating_system` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作系统',
  `useragent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_operate_log
-- ----------------------------
INSERT INTO `hp_operate_log` VALUES (3, 0, '', '/admin/role_add', '新增角色', NULL, 'POST', '127.0.0.1', '后台用户', '{\"role_name\":\"111\",\"status\":\"1\",\"is_cate\":\"0\",\"orders\":\"0\",\"remark\":\"11\"}', 'HTTP/1.0 200 OK\r\nCache-Control: no-cache, private\r\nContent-Type:  application/json\r\nDate:          Fri, 30 Jul 2021 18:07:08 GMT\r\n\r\n{\"code\":0,\"msg\":\"\\u64cd\\u4f5c\\u6210\\u529f\",\"count\":0,\"data\":null}', 0, '操作成功', 'Windows', 'chrome', 1627668428);

-- ----------------------------
-- Table structure for hp_privilege
-- ----------------------------
DROP TABLE IF EXISTS `hp_privilege`;
CREATE TABLE `hp_privilege`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `privilege_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `module_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块名称',
  `controller_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制器名称',
  `action_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名称',
  `route_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路由规则',
  `route_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路由名称',
  `parameter` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '参数',
  `privilege_icon` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限图标',
  `target` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '_self' COMMENT 'target属性',
  `orders` smallint(6) NOT NULL DEFAULT 0 COMMENT '权限排序',
  `is_menu` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否菜单',
  `parent_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '上级权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_privilege
-- ----------------------------
INSERT INTO `hp_privilege` VALUES (1, '常规管理', '#', '#', '#', '#', '#', '', '', '_self', 0, 1, 0);
INSERT INTO `hp_privilege` VALUES (2, '权限管理', '#', '#', '#', '#', '#', '', 'layui-icon-flag', '_self', 0, 1, 1);
INSERT INTO `hp_privilege` VALUES (3, '系统配置', 'admin', 'AdminController', 'system', '/admin/system_config', 'systemConfig', '', 'fa-calendar', '_self', 0, 1, 2);
INSERT INTO `hp_privilege` VALUES (4, '部门管理', 'admin', 'BranchController', 'index', '/admin/branch_list', 'branchList', '', 'fa-address-card-o', '_self', 0, 1, 2);
INSERT INTO `hp_privilege` VALUES (5, '新增部门', 'admin', 'BranchController', 'create', '/admin/branch_add', 'branchAdd', '', '', '_self', 0, 0, 4);
INSERT INTO `hp_privilege` VALUES (6, '编辑部门', 'admin', 'BranchController', 'update', '/admin/branch_edit', 'branchEdit', '', '', '_self', 0, 0, 4);
INSERT INTO `hp_privilege` VALUES (7, '查看部门', 'admin', 'BranchController', 'view', '/admin/branch_view', 'branchView', '', '', '_self', 0, 0, 4);
INSERT INTO `hp_privilege` VALUES (8, '删除部门', 'admin', 'BranchController', 'delete', '/admin/branch_del', 'branchDelete', '', '', '_self', 0, 0, 4);
INSERT INTO `hp_privilege` VALUES (9, '菜单管理', 'admin', 'PrivilegeController', 'index', '/admin/privilege_list', 'privilegeList', '', 'fa-align-justify', '_self', 0, 1, 2);
INSERT INTO `hp_privilege` VALUES (10, '新增菜单', 'admin', 'PrivilegeController', 'create', '/admin/privilege_add', 'privilegeAdd', '', '', '_self', 0, 0, 9);
INSERT INTO `hp_privilege` VALUES (11, '编辑菜单', 'admin', 'PrivilegeController', 'update', '/admin/privilege_edit', 'privilegeEdit', '', '', '_self', 0, 0, 9);
INSERT INTO `hp_privilege` VALUES (12, '删除菜单', 'admin', 'PrivilegeController', 'delete', '/privilege_del', 'privilegeDelete', '', '', '_self', 0, 0, 9);
INSERT INTO `hp_privilege` VALUES (13, '角色管理', 'admin', 'RoleController', 'index', '/admin/role_list', 'roleList', '', 'fa-drivers-license', '_self', 0, 1, 2);
INSERT INTO `hp_privilege` VALUES (14, '新增角色', 'admin', 'RoleController', 'create', '/admin/role_add', 'roleAdd', '', '', '_self', 0, 0, 13);
INSERT INTO `hp_privilege` VALUES (15, '编辑角色', 'admin', 'RoleController', 'update', '/admin/role_edit', 'RoleEdit', '', '', '_self', 0, 0, 13);
INSERT INTO `hp_privilege` VALUES (16, '角色授权', 'admin', 'RoleController', 'authorize', '/admin/role_authorize', 'roleAuthorize', '', '', '_self', 0, 0, 13);
INSERT INTO `hp_privilege` VALUES (17, '删除角色', 'admin', 'RoleController', 'delete', '/admin/role_del', 'roleDelete', '', '', '_self', 0, 0, 13);
INSERT INTO `hp_privilege` VALUES (18, '管理员', 'admin', 'ManagerController', 'index', '/admin/manager_list', 'mangerList', '', 'fa-institution', '_self', 0, 1, 2);
INSERT INTO `hp_privilege` VALUES (19, '新增管理员', 'admin', 'ManagerController', 'create', '/admin/manager_add', 'managerAdd', '', '', '_self', 0, 0, 18);
INSERT INTO `hp_privilege` VALUES (20, '编辑管理员', 'admin', 'ManagerController', 'update', '/admin/manager_edit', 'managerEdit', '', '', '_self', 0, 0, 18);
INSERT INTO `hp_privilege` VALUES (21, '删除管理员', 'admin', 'ManagerController', 'delete', '/admin/manager_del', 'managerDelete', '', '', '_self', 0, 0, 18);
INSERT INTO `hp_privilege` VALUES (22, '日志管理', '#', '#', '#', '#', '', '', 'layui-icon-note', '_self', 0, 1, 1);
INSERT INTO `hp_privilege` VALUES (23, '操作日志', 'admin', 'LogController', 'sysOperateLog', '/admin/sys_operate_log', 'sysOperateLog', '', 'fa-paw', '_self', 0, 1, 22);
INSERT INTO `hp_privilege` VALUES (24, '登录日志', 'admin', 'LogController', 'sysLoginLog', '/admin/sys_login_log', 'sysLoginLog', '', 'fa-calendar-o', '_self', 0, 1, 22);
INSERT INTO `hp_privilege` VALUES (25, '删除操作日志', 'admin', 'LogController', 'deleteSysOperateLog', '/admin/del_operate_log', 'deleteSysOperateLog', '', '', '_self', 0, 0, 22);
INSERT INTO `hp_privilege` VALUES (26, '删除登录日志', 'admin', 'LogController', 'deleteSysLoginLog', '/admin/del_login_log', '', '', '', '_self', 0, 0, 22);

-- ----------------------------
-- Table structure for hp_role
-- ----------------------------
DROP TABLE IF EXISTS `hp_role`;
CREATE TABLE `hp_role`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '启动状态1:启动,0:禁用',
  `is_cate` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否属于分类:1:是,0:否',
  `orders` smallint(6) NOT NULL DEFAULT 0 COMMENT '排序',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注信息',
  `parent_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '上级角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_role
-- ----------------------------
INSERT INTO `hp_role` VALUES (1, '管理员', 1, 1, 0, '', 0);
INSERT INTO `hp_role` VALUES (3, '销售', 1, 0, 0, '', 1);
INSERT INTO `hp_role` VALUES (4, '施工方', 1, 0, 0, '', 1);
INSERT INTO `hp_role` VALUES (5, '总经理', 1, 1, 0, '', 0);
INSERT INTO `hp_role` VALUES (6, '办公室', 1, 0, 0, '', 5);

-- ----------------------------
-- Table structure for hp_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `hp_role_privilege`;
CREATE TABLE `hp_role_privilege`  (
  `role_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '角色id',
  `privilege_id` smallint(6) NOT NULL DEFAULT 0 COMMENT '权限id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hp_role_privilege
-- ----------------------------
INSERT INTO `hp_role_privilege` VALUES (1, 1);
INSERT INTO `hp_role_privilege` VALUES (1, 10);
INSERT INTO `hp_role_privilege` VALUES (6, 33);
INSERT INTO `hp_role_privilege` VALUES (6, 34);
INSERT INTO `hp_role_privilege` VALUES (6, 35);
INSERT INTO `hp_role_privilege` VALUES (6, 36);
INSERT INTO `hp_role_privilege` VALUES (6, 37);
INSERT INTO `hp_role_privilege` VALUES (6, 38);
INSERT INTO `hp_role_privilege` VALUES (6, 39);
INSERT INTO `hp_role_privilege` VALUES (6, 40);
INSERT INTO `hp_role_privilege` VALUES (6, 41);
INSERT INTO `hp_role_privilege` VALUES (6, 42);
INSERT INTO `hp_role_privilege` VALUES (6, 43);
INSERT INTO `hp_role_privilege` VALUES (6, 44);
INSERT INTO `hp_role_privilege` VALUES (6, 45);
INSERT INTO `hp_role_privilege` VALUES (6, 46);
INSERT INTO `hp_role_privilege` VALUES (6, 47);
INSERT INTO `hp_role_privilege` VALUES (6, 48);
INSERT INTO `hp_role_privilege` VALUES (6, 49);
INSERT INTO `hp_role_privilege` VALUES (6, 50);
INSERT INTO `hp_role_privilege` VALUES (6, 51);
INSERT INTO `hp_role_privilege` VALUES (6, 52);
INSERT INTO `hp_role_privilege` VALUES (6, 53);
INSERT INTO `hp_role_privilege` VALUES (6, 54);
INSERT INTO `hp_role_privilege` VALUES (6, 55);
INSERT INTO `hp_role_privilege` VALUES (6, 56);
INSERT INTO `hp_role_privilege` VALUES (6, 57);
INSERT INTO `hp_role_privilege` VALUES (6, 58);
INSERT INTO `hp_role_privilege` VALUES (6, 59);
INSERT INTO `hp_role_privilege` VALUES (6, 60);
INSERT INTO `hp_role_privilege` VALUES (6, 61);
INSERT INTO `hp_role_privilege` VALUES (6, 62);
INSERT INTO `hp_role_privilege` VALUES (6, 63);
INSERT INTO `hp_role_privilege` VALUES (6, 64);
INSERT INTO `hp_role_privilege` VALUES (6, 65);
INSERT INTO `hp_role_privilege` VALUES (6, 66);
INSERT INTO `hp_role_privilege` VALUES (6, 67);
INSERT INTO `hp_role_privilege` VALUES (6, 68);
INSERT INTO `hp_role_privilege` VALUES (6, 69);
INSERT INTO `hp_role_privilege` VALUES (6, 70);
INSERT INTO `hp_role_privilege` VALUES (6, 71);
INSERT INTO `hp_role_privilege` VALUES (6, 72);
INSERT INTO `hp_role_privilege` VALUES (6, 73);
INSERT INTO `hp_role_privilege` VALUES (6, 74);
INSERT INTO `hp_role_privilege` VALUES (6, 75);
INSERT INTO `hp_role_privilege` VALUES (6, 79);
INSERT INTO `hp_role_privilege` VALUES (6, 80);
INSERT INTO `hp_role_privilege` VALUES (6, 81);
INSERT INTO `hp_role_privilege` VALUES (4, 33);
INSERT INTO `hp_role_privilege` VALUES (4, 82);
INSERT INTO `hp_role_privilege` VALUES (4, 83);
INSERT INTO `hp_role_privilege` VALUES (4, 84);
INSERT INTO `hp_role_privilege` VALUES (4, 85);

SET FOREIGN_KEY_CHECKS = 1;
