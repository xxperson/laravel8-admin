<?php
/**
 * Created by PhpStorm.
 * User: ysj
 * Date: 2021/6/21
 * Time: 16:28
 */

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;
use App\Http\Common\Traits\ModelTrait;

class ManagerRole extends BaseModel
{
    use HasFactory;

    use ModelTrait;
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'manager_role';

    /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}