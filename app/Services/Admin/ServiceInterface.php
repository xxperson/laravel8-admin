<?php
/**
 * Created by PhpStorm.
 * User: ysj
 * Date: 2021/6/20
 * Time: 3:37
 */

namespace App\Services\Admin;

interface ServiceInterface
{
    public function add($data);
    public function edit($data,$where);
    public function getList();
}