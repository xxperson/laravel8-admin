## 介绍
基于 Laravel 8 + layui，构架的通用后台开发框架，简洁、易读、方便系统扩展及二次开发。 系统完全开源，数据库文件在public目录下，超管默认为：admin，123456。

## 系统演示
地址：http://www.hepingyl.top/admin

账号：admin
密码：123456

## 仓库地址
https://gitee.com/zhixuan2/laravel8-admin
喜欢的小伙伴，欢迎进群讨论
qq群：749972219

## 内置功能
1.  人员管理：人员是系统操作者，该功能主要完成系统用户配置。
2.  组织架构：配置系统组织机构（公司、部门、小组），树结构展现支持。
3.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
4.  角色管理：角色菜单权限分配、数据权限分配。
5.  登录日志：登录后台记录信息。
6.  参数配置：多种类型的参数配置。

## 界面截图

![laravel8 + layui 开发通用后台](https://cdn.learnku.com/uploads/images/202207/26/95026/k3KDemwQYv.png!large)



![laravel8 + layui 开发通用后台](https://cdn.learnku.com/uploads/images/202207/26/95026/5me5bZFPHw.png!large)



![laravel8 + layui 开发通用后台](https://cdn.learnku.com/uploads/images/202207/26/95026/mh1OqAxsMc.png!large)


![laravel8 + layui 开发通用后台](https://cdn.learnku.com/uploads/images/202207/26/95026/56G0oLzlPB.png!large)


![laravel8 + layui 开发通用后台](https://cdn.learnku.com/uploads/images/202207/26/95026/Zp047rQs61.png!large)
